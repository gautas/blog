.PHONY: dev_image dev_container book serve test bump

dev_image:
	docker build -t blog -f dev.dockerfile .

dev_container:
	docker run -it --rm -d --name blog --env-file .env.container -v $(shell pwd):/blog blog

book:
	jb build book

serve:
	python -m http.server -d book/_build/html $(port)

test:
	pytest

bump:
	poetry version minor
