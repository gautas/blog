"""
package utilities
"""


def add(a: int, b: int) -> int:
    """add two numbers

    Args:
        a (int): a number
        b (int): a number

    Returns:
        int: the sum of the two numbers
    """
    return a + b